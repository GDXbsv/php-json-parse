<?php
declare(strict_types = 1);

/**
 * @param string $filePath
 */
function parse(string $filePath)
{
    $stack = new SplStack();
    //we saved inputstream to file
    $jsonFile = new SplFileObject($filePath);

    $offsetsCollection = [];
    $jsonFile->rewind();
    while (false !== ($char = $jsonFile->fgetc())) {
        switch ($char) {
            case ('{'): {
                $stack->push($jsonFile->ftell() - 1);
                break;
            }
            case ('}'): {
                $prevOffset = $stack->pop();
                if ($stack->count() === 0) {
                    $curOffset = $jsonFile->ftell();
                    $jsonFile->fseek($prevOffset);
                    $jsonObjectString = $jsonFile->fread($curOffset - $prevOffset);
                    $jsonFile->fseek($curOffset);
                    $offsetsCollection = getOffsetsToRemove($jsonObjectString, $prevOffset, $offsetsCollection);
                }
                break;
            }
            default: {
                break;
            }
        }
    }

    //sort $usersSkippedOffsets
    $skippedOffsetsHeap = new SplMinHeap();
    foreach ($offsetsCollection as $offsets) {
        foreach ($offsets as $offset) {
            $skippedOffsetsHeap->insert($offset);
        }
    }
    unset($offsetsCollection);

    $jsonFile->rewind();
    $skippedOffsetsHeap->rewind();
    if ($skippedOffsetsHeap->count() === 0) {
        echo $jsonFile->fread($jsonFile->getSize());
    } else {
        foreach ($skippedOffsetsHeap as list($start, $end)) {
            $curPos = $jsonFile->ftell();
            if ($curPos < $start) {
                echo $jsonFile->fread($start - $jsonFile->ftell());
            }
            $jsonFile->fseek($end);
            skipSymbols($jsonFile);
            $jsonFile->fseek(-1, SEEK_CUR);
        }
        echo $jsonFile->fread($jsonFile->getSize());
    }

}

/**
 * @param SplFileObject $jsonFile
 *
 * @return string
 */
function skipSymbols(SplFileObject $jsonFile)
{
    $char = $jsonFile->fgetc();
    if ($char == ',' || $char === ' ' || $char === "\n") {
        skipSymbols($jsonFile);
    }
}
