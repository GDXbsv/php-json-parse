<?php
declare(strict_types = 1);

require_once 'functions.php';

/**
 * @param string $jsonObjectString
 *
 * @param int    $initOffset
 *
 * @param array  $offsetsCollection
 *
 * @return array
 */
function getOffsetsToRemove(string $jsonObjectString, int $initOffset, array $offsetsCollection): array
{
    global $usersScore;
    if (!isset($usersScore)) {
        $usersScore = [];
    }

    $jsonUser = json_decode($jsonObjectString, true);
    $userId = $jsonUser['user'];
    $userScore = $jsonUser['score'];
    $userType = $jsonUser['type'];
    if ($userType === 'yyy'
        && (!array_key_exists($userId, $usersScore) || $userScore <= $usersScore[$userId])
    ) {
        if (!array_key_exists($userId, $usersScore)) {
            $usersScore[$userId] = $userScore;
        }
        $startOffset = $initOffset;
        $endOffset = $startOffset + strlen($jsonObjectString);
        $offsets = [$startOffset, $endOffset];
        if ($userScore < $usersScore[$userId]) {
            $offsetsCollection[$userId] = [$offsets];
        } else {
            $offsetsCollection[$userId][] = $offsets;
        }
        $usersScore[$userId] = $userScore;
    }

    return $offsetsCollection;
}

parse('one.json');
