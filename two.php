<?php
declare(strict_types = 1);

require_once 'functions.php';

/**
 * @param string $jsonObjectString
 *
 * @param int    $initOffset
 *
 * @param array  $offsetsCollection
 *
 * @return array
 */
function getOffsetsToRemove(string $jsonObjectString, int $initOffset, array $offsetsCollection): array
{
    $skippedOffsets = [];
    $jsonUser = json_decode($jsonObjectString, true);

    $minTime = PHP_INT_MAX;
    foreach ($jsonUser['occupations'] as $occupation) {
        $activityLevel = $occupation['activityLevel'];
        $time = $occupation['time'];
        $title = $occupation['title'];
        if ($activityLevel === 0 && $time <= $minTime) {
            preg_match("/\{.+\"$title\".+$time.+\}/", $jsonObjectString, $matches, PREG_OFFSET_CAPTURE);
            $startOffset = $matches[0][1] + $initOffset;
            $endOffset = $startOffset + strlen($matches[0][0]);
            $offsets = [$startOffset, $endOffset];
            if ($time < $minTime) {
                $skippedOffsets = [$offsets];
            } else {
                $skippedOffsets[] = $offsets;
            }
            $minTime = $time;
        }
    }

    $offsetsCollection[] = $skippedOffsets;

    return $offsetsCollection;
}

parse('two.json');
